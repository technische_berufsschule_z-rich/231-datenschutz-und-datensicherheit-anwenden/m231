# Checklisten des Datenschutzbeauftragten des Kantons Zürichs

# Checkliste Smartphone-Sicherheit erhöhen
 - Link: https://www.datenschutz.ch/meine-daten-schuetzen/smartphone-sicherheit-erhoehen
 - 01 Gerätesperren aktivieren
    - Ja, ich habe eine Gerätesperre mit PIN aktiviert. 
 - 02 Gerät bei Verlust sofort sperren und löschen lassen.
    - Kannte ich noch nicht, habe https://www.google.com/android/find besucht und mir die Funktionen angeschaut. Jetzt weiss ich wie ich im Ernstfall handeln muss. 
 - 03 Apps aus vertrauenswürdigen Quellen installieren
    - Ich installiere alle Apps aus dem Google Playstore und prüfe bei jeder App, welche Zugriffsrechte Sie hat. Ich bin alle Berechtigungseinstellungen für jedes App im meinem Telefon durchgegangen und wenn nötig Änderungen vorgenommen
 - 04 öffentliche Funknetzwerke mit Vorsicht nutzen 
 
   - ICh passe immer auf bei öffentlichen und besorge mir mein eigenes 4G damit ich weniger auf potenziel gefährliche Netzwerke angewiesen bin.

-  05 Updates regelmässig durchführen
    -  Update mach ich sobald ich die Möglichkeit   dazu habe.

- 06 Daten vor Entsorgung oder Verkauf komplett löschen 
  - Ich lösche immer alles oder vernichte sogar meine alten geräte bevor ich sie wegwerfe.

- 07 drahtlose Schnittstellen immer trennen.
  - ich trenne immer das Wlan wenn ich es nicht mehr brauche.



#PC Sicherheit erhöhen in 5 Schritten 

- 01 persönliche Informationen schützen 
  - Ich gebe nicht mehr Infos Preis als nötig.ich lösche Temporäre Daten im Cache und den Verlauf regelmässig. In Zukunft versuche ich anonym zu surfen. 

- 02 Angriffe abwehren
   - ich führe regelmässig updates durch die zur Sicherheit beitragen.Ich benutze firewalls und sichere Browsereinstellungen.Ich installiere Virenschutzprogramme. 

- 03 Zugriffe unberechtigter verhindern
   - ich verwende sichere Passwörter.Nutze Drahtlose Netzwerke mit Vorsicht. Nutzen sie eine Bildschirmsperre.

- 04 Verschlüsseln sie sensitive Inhalte 
     -Ich verschlüssle Datenträger und Emails mit sensitivem Inhalt.

- 05 Suchen sie Informationen und löschen sie Daten vollständig 
   - Ich führe regelmässige Datensicherungen durch.Ich bewahre Sicherheitskopien Feuer und Diebstahlsicher durch. Vor Verkauf oder Entsorgung des Pcs lösche ich alles mit Hilfe spezieller Software. 